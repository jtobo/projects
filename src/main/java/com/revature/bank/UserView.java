package com.revature.bank;



import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.revature.bank.beans.AccountBean;
import com.revature.services.BankServices;
import com.revature.services.CustomerServices;
import com.revature.bank.beans.CustomerBean;

public class UserView {
	BankServices bs=new BankServices();
	Scanner scan = new Scanner(System.in);
	CustomerServices cs=new CustomerServices();
	CustomerBean cb= new CustomerBean();
	AccountBean ab= new AccountBean();
	Integer user;
	boolean userIn=false;
	boolean retUser=false;
	
		public void beginning() {
			System.out.println("Welcome to CryptoBank"
					+ "\n 1.Sign in."
					+ "\n       or"
					+ "\n 2.Register as a new client.\n");


		    try{
		    	user= Integer.parseInt(scan.next());
		    }catch(NumberFormatException ex){ // handle your exception
		       System.out.println("Not a number, try again");
		       beginning();
		    }


			
			switch (user)
			{
				case 1:
					existingAccount();					
					break;
		
				case 2:
					registerAccountType();					
					break;
				default:
					System.out.println("Incorrect choice");
					beginning();
			}
		}
		
		public void registerAccountType() {
			System.out.println(" Would you like to sign up for a "
					+ "\n 1.Standard account ( Checking and Savings)"
					+ "\n            or "
					+ "\n 2.Joint account");
			try{
		    	user= Integer.parseInt(scan.next());
		    }catch(NumberFormatException ex){ // handle your exception
		       System.out.println("Not a number, try again");
		       registerAccountType();
		    }
			switch (user) {
			case 1:
				registerCredentials();
				break;
			case 2:
				joinExistingAccount();
				break;
			default:
				System.out.println("Incorrect choice");
				registerAccountType();
				
			}
		}
		public void existingAccount() {
			String username=null;
			String password=null;
			System.out.println("Please type in the username associated with the account \n");
			username= scan.next();
			if(cs.checkUsername(username)==true)
			{
				//checkPassword();
				System.out.println("Type the password associated with the username \n");
				password=scan.next();
				if(cs.checkPassword(password, username)==true) {
					accountMenu(username,password);
				}
				else{
					System.out.println("Wrong password returning to main screen");
					beginning();
				}
			}
			else {
				System.out.println("Username not found in database!");
				beginning();
			}
		}
		public void checkPassword() {
			System.out.println("Type the password associated with the username \n");
			// check if password is correct
			//else go back to registerAccount
		}
		public void accountMenu(String username, String password) {
				cb=cs.getUserDetails(username,password );
				AccountBean ab= new AccountBean();
				userIn=false;
				double amount1=0;
				int CheckSaving1=0;
				int CheckSaving2=0;
				int accountIdTo=0;
				int accountIdFrom=0;
				List<String> accounts=new ArrayList<String>();
				List<AccountBean>l=new ArrayList<AccountBean>();
				//before any of the account menu happens check to see if the customer still has an active open account:
				if(bs.viewAccounts(cb.getCustomerId()).isEmpty()==true){
					System.out.println("You are a returning customer with no open account."
							+ "\n Please open one now");
					createAccountReturning(username, password);
				}
				System.out.println("What would you like to do "
						+ cb.getFirstName()+" "+cb.getLastName()+"\n"
						+ " 1. View Accounts \n 2. Deposit "
						+ "\n 3. Withdraw \n 4. Check Available Balance "
						+ "\n 5. Transfer to another account  \n 6. Close account \n 7. Create New Account"
							+ "\n 8. Log out");
				try{
			    	user= Integer.parseInt(scan.next());
			    }catch(NumberFormatException ex){ // handle your exception
			       System.out.println("Not a number, try again");
			       accountMenu(username, password);
			    }
				switch (user) {
					case 1:
						
						accounts=bs.viewAccounts(cb.getCustomerId());
						System.out.println("Account Numbers associated: ");// +accounts.toString());
						for ( String id : accounts) 
						      System.out.println( id);
						otherTransaction(username, password);
						break;
					case 2:
						
						System.out.println("Where would you like to deposit money: \n"
								+ "1. Checkings \n 2. Savings");
							CheckSaving1= Integer.parseInt(scan.next());
							System.out.println("How much money would you like to deposit: ");
							amount1= Double.parseDouble(scan.next());
							switch(CheckSaving1) {
								case 1:
									System.out.println("Which account ID would you like to deposit the money");
									accounts=bs.viewAccounts(cb.getCustomerId());
									System.out.println(accounts);
									int accountId=Integer.parseInt(scan.next());
									bs.depositChecking(amount1, accountId);
									System.out.println("Sucessfully deposited "+amount1);
									otherTransaction(username, password);
									break;
								case 2:
									System.out.println("Which account ID would you like to deposit the money");
									accounts=bs.viewAccounts(cb.getCustomerId());
									System.out.println(accounts); 
									accountId=Integer.parseInt(scan.next());
									bs.depositSaving(amount1, accountId);
									System.out.println("Sucessfully deposited "+amount1);
									otherTransaction(username, password);
									break;
								default:
									System.out.println("Incorrect Option going back to account menu");
									accountMenu(username,password);
							}
						break;
					case 3:
						System.out.println("Where would you like to withdraw money from: \n"
								+ "1. Checkings \n 2. Savings");
						l=bs.checkBalance(Integer.toString(cb.getCustomerId()));
						for ( AccountBean bean : l ) 
						      System.out.println( bean);
							CheckSaving1= Integer.parseInt(scan.next());
							System.out.println("How much money would you like to withdraw: ");
							amount1= Double.parseDouble(scan.next());
							switch(CheckSaving1) {
							case 1:
								System.out.println("Which account ID would you like to withdraw the money from");
								int accountId=Integer.parseInt(scan.next());
								//cb.getCustomerId()
								if(bs.accountIsAssociated(cb.getCustomerId(), accountId)==false) {
									System.out.println(cb.getCustomerId()+" "+accountId);
									System.out.println("Do not have permission to withdraw from there"
											+ " Returning to account menu.");
									accountMenu(username, password);
									
								}
								ab=bs.getBalance(accountId);
								if (ab.getCheckings()<amount1) {
									System.out.println("Amount trying to withdraw is far more than available balance"
											+ "\n Returning to account menu");
									accountMenu(username, password);
									
								}
								bs.withdrawChecking(amount1, accountId);
								System.out.println("Sucessfully withdrew "+amount1);
								otherTransaction(username, password);
								
								break;
							case 2:
								System.out.println("Which account ID would you like to withdraw the money from");
								 accountId=Integer.parseInt(scan.next());
								 if(bs.accountIsAssociated(cb.getCustomerId(), accountId)==false) {
										System.out.println(cb.getCustomerId()+" "+accountId);
										System.out.println("Do not have permission to withdraw from there"
												+ " Returning to account menu.");
										accountMenu(username, password);
									}
								 ab=bs.getBalance(accountId);
									if (ab.getSavings()<amount1) {
										System.out.println("Amount trying to withdraw is far more than available balance"
												+ "\n Returning to account menu");
										accountMenu(username, password);
										
									}
								bs.withdrawSaving(amount1, accountId);
								System.out.println("Sucessfully withdrew "+amount1);
								otherTransaction(username, password);
								break;
							default:
								System.out.println("Incorrect Option going back to account menu");
								accountMenu(username,password);
						}
						break;
					case 4:
						l=bs.checkBalance(Integer.toString(cb.getCustomerId()));
						for ( AccountBean bean : l ) 
						      System.out.println( bean);
						otherTransaction(username, password);
						break;
					case 5:
						l=bs.checkBalance(Integer.toString(cb.getCustomerId()));
						for ( AccountBean bean : l ) 
						      System.out.println( bean);
							System.out.println("Where would you like to transfer money from: \n"
								+ "1. Checkings \n 2. Savings");
							CheckSaving1= Integer.parseInt(scan.next());
							System.out.println("How much money would you like to transfer: ");
							amount1= Double.parseDouble(scan.next());
							System.out.println("Where would you like to transfer to: \n"
									+ "1. Checkings \n 2. Savings");
							CheckSaving2= Integer.parseInt(scan.next());
							if(CheckSaving1==1 &&CheckSaving2==1) {
								System.out.println("Type in the account id you wish to transfer from");
								 accountIdFrom=Integer.parseInt(scan.next());
								ab=bs.getBalance(accountIdFrom);
								if(ab== null | ab.getCheckings()<amount1) {
									System.out.println("Cannot be done, does not exist or insufficient funds, going back to account menu");
									accountMenu(username,password);
								}
							System.out.println("Type account you wish to transfer the money to");
								accountIdTo=Integer.parseInt(scan.next());
								bs.transferCC(accountIdFrom, accountIdTo, amount1);
								otherTransaction(username, password);
							}
							else if (CheckSaving1==1 &&CheckSaving2==2) {
								System.out.println("Type in the account id you wish to transfer from");
								 accountIdFrom=Integer.parseInt(scan.next());
								ab=bs.getBalance(accountIdFrom);
								if(ab== null | ab.getCheckings()<amount1) {
									System.out.println("Cannot be done, does not exist or insufficient funds, going back to account menu");
									accountMenu(username,password);
								}
							System.out.println("Type account you wish to transfer the money to");
								accountIdTo=Integer.parseInt(scan.next());
								bs.transferCS(accountIdFrom, accountIdTo, amount1);
								otherTransaction(username, password);
							}
							else if(CheckSaving1==2 &&CheckSaving2==1) {
								System.out.println("Type in the account id you wish to transfer from");
								 accountIdFrom=Integer.parseInt(scan.next());
								ab=bs.getBalance(accountIdFrom);
								if(ab== null | ab.getSavings()<amount1) {
									System.out.println("Cannot be done, does not exist or insufficient funds, going back to account menu");
									accountMenu(username,password);
								}
							System.out.println("Type account you wish to transfer the money to");
								accountIdTo=Integer.parseInt(scan.next());
								bs.transferSC(accountIdFrom, accountIdTo, amount1);
								otherTransaction(username, password);
							}
							else if(CheckSaving1==2 &&CheckSaving2==1) {
								System.out.println("Type in the account id you wish to transfer from");
								 accountIdFrom=Integer.parseInt(scan.next());
								ab=bs.getBalance(accountIdFrom);
								if(ab== null | ab.getSavings()<amount1) {
									System.out.println("Cannot be done, does not exist or insufficient funds, going back to account menu");
									accountMenu(username,password);
								}
							System.out.println("Type account you wish to transfer the money to");
								accountIdTo=Integer.parseInt(scan.next());
								bs.transferSS(accountIdFrom, accountIdTo, amount1);
								otherTransaction(username, password);
							}
							else {
								System.out.println(" Not an option going back to account menu");
								accountMenu(username,password);
							}
						break;
					case 6:
						closeAccount(cb.getCustomerId(),username,password);
						break;
					case 7:
						createAccountReturning(username,password);
					case 8:
						System.out.println("Have a nice day!");
						beginning();
						break;
					default:
						System.out.println("Not an option");
						accountMenu(username,password);
				}	
		}
		public void registerCredentials() {
			String username=null;
			String firstName=null;
			String lastName=null;
			String password=null;
			CustomerBean cb=new CustomerBean();
			
			System.out.println("Please type your First name");
			firstName=scan.next();
			System.out.println("Please type your Last name");
			lastName=scan.next();
			System.out.println(" Please give us a username to use with your account");
			//check to see if it already exists if yes ask again
			username=scan.next();
			if (cs.checkUsername(username)==true) {
				System.out.println("Username already exist, will need to register credentials");
				registerCredentials();
			}
			
			
			System.out.println("Please give us a password to go along with your username");
			password=scan.next();
			cs.newAccount(firstName, lastName, username, password);
			cb=cs.getUserDetails(username, password);
			bs.newAccount(cb.getCustomerId());
			beginning();
		}
		public void joinExistingAccount() {
			System.out.println("Type the account id you would like to join");
			int accountId= Integer.parseInt(scan.next());
			if (bs.accountExists(accountId)==false) {
				System.out.println("No account id was found in database");
				joinExistingAccount();
			}
			
			System.out.println(" Please have the owner(s) type a password associated with the "
																+ "account to approve it.");
			String password=scan.next();
			if(bs.verifyJoint(password,accountId)==true) {
				if(retUser==true) {
					System.out.println("Will need to sign in again so changes could take effect");
					cs.includeToAccount(cb.getCustomerId(),accountId);
					beginning();
				}
				String username=null;
				String firstName=null;
				String lastName=null;
				String password1=null;
				System.out.println("You joined the account");
				System.out.println("Please type your First name");
				firstName=scan.next();
				System.out.println("Please type your Last name");
				lastName=scan.next();
				System.out.println(" Please give us a username to use with your account");
				username=scan.next();
				if (cs.checkUsername(username)==true) {
					System.out.println("Username already exist, will need to join account again");
					joinExistingAccount();
				}
				
				
				System.out.println("Please give us a password to go along with your username");
				password1=scan.next();
				//CustomerBean cb= new CustomerBean();
				cs.addUser(firstName, lastName, username, password1);
				cb= cs.getUserDetails(username, password1);
				cs.includeToAccount(cb.getCustomerId(),accountId);
				System.out.println("Succesfully joined the account:"+accountId);
				otherTransaction(username, password1);
			}
			else {
				System.out.println("Wrong password ");
				existingAccount();
			}
		}
		public void closeAccount(int customerId,String username, String password) {
			List<String> accounts=new ArrayList<String>();
			System.out.println("Which account would you like to close");
			accounts=bs.viewAccounts(customerId);
			for ( String bean : accounts ) 
			      System.out.println( bean);
			int accountId=Integer.parseInt(scan.next());
			if(bs.accountExists(accountId)==true) {
				double sum=0;
				AccountBean ab= new AccountBean();
				
				//CHECK TO SEE IF ITS a JOINT ACCOUNT 
				
				if(bs.checkIfJoint(accountId,customerId)==true) {
					System.out.println("Looks like there is another customer associated with it. "
							+ "We will remove your customer Id from the account. ");
					//if(bs.removeCustomer(accountId,customerId)==false) {
						bs.close(accountId, customerId);
						beginning();
					//}
				}
//------------------------------				
				else {
						ab=bs.getBalance(accountId);
						System.out.println("Before closing it will withdraw both checking and savings.");
						sum= ab.getCheckings()+ab.getSavings();
						System.out.println("Withdrew a total of : "+ sum);
						bs.withdrawChecking(ab.getCheckings(), accountId);
						bs.withdrawSaving(ab.getSavings(), accountId);
						System.out.println("Closing account");
						bs.close(accountId,customerId);
						beginning();
				}
			}
			else {
				System.out.println("Account id not associated with your customer id or does not exist."
						+ "\n Returning to account menu");
				accountMenu(username, password);
						
			}	
		}
		public void customerLogOut() {
			System.out.println("Goodbye!");
			beginning();
			
		}
		public void otherTransaction(String username,String password) {
			System.out.println("Would you like to do another transaction? "
					+ "\n 1. Yes \n 2. No ");
			user= Integer.parseInt(scan.next());
			switch(user) {
			case 1:
				accountMenu(username,password);
				break;
			case 2:
				customerLogOut();
				break;
			default:
				otherTransaction(username,password);
			}
		}
		public void createAccountReturning(String username,String password) {
			System.out.println(" Which account would you like to create:"
					+ "\n 1.Standard account ( Checking and Savings)"
					+ "\n            or "
					+ "\n 2.Joint account");
			try{
		    	user= Integer.parseInt(scan.next());
		    }catch(NumberFormatException ex){ // handle your exception
		       System.out.println("Not a number, try again");
		       createAccountReturning(username, password);
		    }
			switch (user) {
			case 1:
				bs.registerReturningCredentials(cb.getCustomerId());
				System.out.println("New standard account succesfully created");
				accountMenu(username,password);
				break;
			case 2:
				retUser=true;
				cb=cs.getUserDetails(username, password);
				joinExistingAccount();
				System.out.println("Successfully joined the account");
				accountMenu(username,password);
				break;
			default:
		}
		}
}
