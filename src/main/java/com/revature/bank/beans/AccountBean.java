package com.revature.bank.beans;

public class AccountBean {
String accountId;
double savings;
double checkings;

public AccountBean(String accountId, int savings, int checkings) {
	super();
	this.accountId = accountId;
	this.savings = savings;
	this.checkings = checkings;
}
public AccountBean() {
	super();
	// TODO Auto-generated constructor stub
}

public String getAccountId() {
	return accountId;
}
public void setAccountId(String accountId) {
	this.accountId = accountId;
}
public double getSavings() {
	return savings;
}
@Override
public String toString() {
	return "AccountBean [accountId=" + accountId + ", savings=" + savings + ", checkings=" + checkings + "]";
}
public void setSavings(double savings) {
	this.savings = savings;
}
public double getCheckings() {
	return checkings;
}
public void setCheckings(double amount) {
	this.checkings = amount;
}





}
