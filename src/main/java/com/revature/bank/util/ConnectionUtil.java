package com.revature.bank.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



public class ConnectionUtil {
	private static ConnectionUtil connectionUtil= new ConnectionUtil();
	
	private ConnectionUtil() {}
	
	public static ConnectionUtil getConnectionUtil() {
		return connectionUtil;
	}
	
	public Connection getConnection() {

		try {
			//get env variable from properties
			String url= System.getenv("URL_BANK");
			String user= System.getenv("USER_SQL");
			String password= System.getenv("PASSWORD_SQL");

			// get connection using properties data
			return DriverManager.getConnection(url, user, password);
			
		}catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
}
