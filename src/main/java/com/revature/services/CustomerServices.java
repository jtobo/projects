package com.revature.services;

import com.revature.bank.beans.CustomerBean;
import com.revature.daos.CustomerDao;
import com.revature.daos.CustomerDaoImpl;

public class CustomerServices {
	 private CustomerDao dao = new CustomerDaoImpl();
	public CustomerBean getUserDetails(String username, String password) {
		CustomerBean cb= new CustomerBean();
		cb=dao.getUserDetails(username, password);
		cb.setUsername(username);
		cb.setPassword(password);
		return cb;
	}
	public boolean checkUsername(String username) {
		CustomerBean cb= new CustomerBean();
		cb.setUsername(username);
		return dao.checkUsername(username);
	}
	public boolean checkPassword(String password ,String username) {
		CustomerBean cb= new CustomerBean();
		cb.setPassword(password);
		cb.setUsername(username);
		return dao.checkPassword(password,username);
	}
	public CustomerBean addUser(String firstName,String lastName,String username,String password) {
		CustomerBean cb= new CustomerBean();
		cb.setFirstName(firstName);
		cb.setLastName(lastName);
		cb.setUsername(username);
		cb.setPassword(password);
		dao.addUser(firstName, lastName, username, password);
		return cb;
	}
	public void newAccount(String firstName,String lastName, String username, String password) {
		 dao.newAccount(firstName, lastName, username, password);
	}

	public void includeToAccount(int customerId,int accountId) {
		dao.includeToAccount(customerId, accountId);
	}
}
