package com.revature.services;


import java.util.List;
import com.revature.bank.beans.AccountBean;
import com.revature.daos.BankDao;
import com.revature.daos.BankDaoImpl;

public class BankServices {
	private BankDao dao= new BankDaoImpl();
	
	public List<String> viewAccounts(int customerId)
	{
		//List<String> accounts= new ArrayList<String>();
		return dao.viewAccounts(customerId);
	}
	
	public void depositChecking(double amount,int accountId) {
		dao.depositChecking(amount, accountId);
	}
	public void depositSaving(double amount,int accountId) {
		dao.depositSaving(amount, accountId);
	}
	public void withdrawChecking(double amount,int accountId) {
		dao.withdrawChecking(amount, accountId);
	}
	public void withdrawSaving(double amount,int accountId) {
		dao.withdrawSaving(amount, accountId);
	}
	public List<AccountBean> checkBalance(String customerId) {
		return dao.checkBalance(customerId);
	}
	public AccountBean getBalance(int accountId) {
		return dao.getBalance(accountId);
	}
	public void transferCC(int accountFro, int accountTo, double amountFro) {
		
		dao.transferCC(accountFro, accountTo, amountFro);	
	}
	public void transferCS(int accountFro, int accountTo, double amountFro) {
		
		dao.transferCS(accountFro, accountTo, amountFro);	
	}
	public void transferSC(int accountFro, int accountTo, double amountFro) {
		
		dao.transferSC(accountFro, accountTo, amountFro);	
	}
	public void transferSS(int accountFro, int accountTo,double amountFro) {
		
		dao.transferSS(accountFro, accountTo, amountFro);	
	}
	public void close(int accountId,int customerId) {
		dao.close(accountId,customerId);
	}
	public void newAccount(int customerId ) {
		dao.newAccount(customerId);
	}
	public boolean accountExists(int accountId) {
		return dao.accountExists(accountId);
	}
	public boolean verifyJoint(String password,int accountId) {
		return dao.verifyJoint(password,accountId);
	}
	public boolean checkIfJoint(int accountId,int customerId) {
		return dao.checkIfJoint(accountId,customerId);
	}
	public void registerReturningCredentials(int customerId) {
		dao.registerReturningCredentials(customerId);
	}
	public boolean accountIsAssociated(int customerId,int accountId) {
		return dao.accountIsAssociated(customerId,accountId);
	}
		
}
