package com.revature.daos;

import com.revature.bank.beans.CustomerBean;

public interface CustomerDao {
	public CustomerBean getUserDetails(String username, String password);
	public boolean checkUsername(String username);
	public CustomerBean addUser(String firstName,String lastName,String username,String password);
	public boolean checkPassword(String password,String username);
	public void newAccount(String firstName,String lastName, String username, String password);
	public void includeToAccount(int customerId,int accountId);
	}
