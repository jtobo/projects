package com.revature.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.revature.bank.beans.AccountBean;
import com.revature.bank.util.ConnectionUtil;


public class BankDaoImpl implements BankDao  {
	private ConnectionUtil connectionUtil= ConnectionUtil.getConnectionUtil();
	
	
	private AccountBean extractAccount(ResultSet results) throws SQLException {
		AccountBean a = new AccountBean();
		a.setAccountId(results.getString("account_id"));
		a.setCheckings(results.getDouble("checkings"));
		a.setSavings(results.getDouble("savings"));
		return a;
	}
	
	@Override
	public List<String> viewAccounts(int customerId) {
		List<String> accounts= new ArrayList<String>();
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "SELECT account_id FROM customer_account WHERE customer_id=? ORDER BY account_id";
			PreparedStatement ps=conn.prepareStatement(sql);
			ps.setInt(1, customerId);
			ResultSet results=ps.executeQuery();
			while (results.next()) {
				accounts.add(results.getString("account_id"));
			}
			return accounts;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void depositChecking(double amount,int accountId ) {
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "UPDATE account_info SET checkings =((SELECT checkings from account_info where account_id=?) +?) WHERE account_id=?";
			PreparedStatement ps= conn.prepareStatement(sql);
			ps.setInt(1, accountId);
			ps.setDouble(2, amount);
			ps.setInt(3, accountId);
			 ps.executeUpdate();
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//return null;
	}
	
	@Override
	public void depositSaving(double amount,int accountId ) {
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "UPDATE account_info SET savings =((SELECT savings from account_info where account_id=?) +?) WHERE account_id=?";
			PreparedStatement ps= conn.prepareStatement(sql);
			ps.setInt(1, accountId);
			ps.setDouble(2, amount);
			ps.setInt(3, accountId);
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void withdrawChecking(double amount, int accountId) {
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "UPDATE account_info SET checkings=((SELECT checkings from account_info where account_id=?) -?) WHERE account_id=?";
			PreparedStatement ps=conn.prepareStatement(sql);
			ps.setInt(1, accountId);
			ps.setDouble(2, amount);
			ps.setInt(3, accountId);
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void withdrawSaving(double amount, int accountId) {
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "UPDATE account_info SET savings=((SELECT savings from account_info where account_id=?) -?) WHERE account_id=?";
			PreparedStatement ps=conn.prepareStatement(sql);
			ps.setInt(1, accountId);
			ps.setDouble(2, amount);
			ps.setInt(3, accountId);
			ps.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<AccountBean> checkBalance(String customerId) {
		List<AccountBean> ab= new ArrayList<AccountBean>();
		try(Connection conn= connectionUtil.getConnection()){
			String sql="SELECT ai.account_id, ai.checkings,ai.savings FROM account_info ai  " + 
					" JOIN customer_account ca ON ai.account_id=ca.account_id " + 
					" WHERE ca.customer_id=? ORDER BY ai.account_id";
			PreparedStatement ps= conn.prepareStatement(sql);
			ps.setInt(1, Integer.parseInt(customerId));
			ResultSet results= ps.executeQuery();
			while (results.next()) {
				ab.add(extractAccount(results));
			}
			return ab;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public AccountBean getBalance(int accountId) {
		AccountBean ab= new AccountBean();
		try(Connection conn= connectionUtil.getConnection()){
			String sql="SELECT account_id,checkings,savings FROM account_info WHERE account_id=?";
			PreparedStatement ps= conn.prepareStatement(sql);
			ps.setInt(1, accountId);
			ResultSet results= ps.executeQuery();
			while (results.next()) {
				ab=extractAccount(results);
			}
			return ab;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public void transferCC(int accountFro, int accountTo, double amountFro) {
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "UPDATE account_info SET checkings =((SELECT checkings from account_info where account_id=?) -?)" + 
					"WHERE account_id=?;" + 
					"UPDATE account_info SET checkings=((SELECT checkings from account_info where account_id=?) +?)" + 
					"WHERE account_id=?";
			PreparedStatement ps=conn.prepareStatement(sql);
			ps.setInt(1, accountFro);
			ps.setDouble(2,amountFro);
			ps.setInt(3, accountFro);
			ps.setInt(4, accountTo);
			ps.setDouble(5, amountFro);
			ps.setInt(6, accountTo);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void transferCS(int accountFro, int accountTo, double amountFro) {
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "UPDATE account_info SET checkings =((SELECT checkings from account_info where account_id=?) -?)" + 
					"WHERE account_id=?;" + 
					"UPDATE account_info SET savings=((SELECT savings from account_info where account_id=?) +?)" + 
					"WHERE account_id=?";
			PreparedStatement ps=conn.prepareStatement(sql);
			ps.setInt(1, accountFro);
			ps.setDouble(2,amountFro);
			ps.setInt(3, accountFro);
			ps.setInt(4, accountTo);
			ps.setDouble(5, amountFro);
			ps.setInt(6, accountTo);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void transferSC(int accountFro, int accountTo, double amountFro) {
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "UPDATE account_info SET savings =((SELECT savings from account_info where account_id=?) -?)" + 
					"WHERE account_id=?;" + 
					"UPDATE account_info SET checkings=((SELECT checkings from account_info where account_id=?) +?)" + 
					"WHERE account_id=?";
			PreparedStatement ps=conn.prepareStatement(sql);
			ps.setInt(1, accountFro);
			ps.setDouble(2,amountFro);
			ps.setInt(3, accountFro);
			ps.setInt(4, accountTo);
			ps.setDouble(5, amountFro);
			ps.setInt(6, accountTo);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void transferSS(int accountFro, int accountTo, double amountFro) {
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "UPDATE account_info SET savings =((SELECT savings from account_info where account_id=?) -?)" + 
					"WHERE account_id=?;" + 
					"UPDATE account_info SET checkings=((SELECT checkings from account_info where account_id=?) +?)" + 
					"WHERE account_id=?";
			PreparedStatement ps=conn.prepareStatement(sql);
			ps.setInt(1, accountFro);
			ps.setDouble(2,amountFro);
			ps.setInt(3, accountFro);
			ps.setInt(4, accountTo);
			ps.setDouble(5, amountFro);
			ps.setInt(6, accountTo);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void close(int accountId,int customerId) {
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "INSERT INTO account_closed (customer_id,account_id)\r\n" + 
					"VALUES ((SELECT customer_id FROM customer_account WHERE customer_id=? AND account_id=?),\r\n" + 
					"		(SELECT account_id FROM customer_account WHERE customer_id=? AND account_id=?))";
			PreparedStatement ps= conn.prepareStatement(sql);
			ps.setInt(1, customerId);
			ps.setInt(2, accountId);
			ps.setInt(3, customerId);
			ps.setInt(4, accountId);
			ps.execute();
			String sql2= "DELETE FROM customer_account \r\n" + 
					"WHERE customer_id=? AND account_id=?";
			PreparedStatement ps2= conn.prepareStatement(sql2);
			ps2.setInt(1, customerId);
			ps2.setInt(2, accountId);
			ps2.execute();
		}	catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}

	
	@Override
	public void newAccount(int customerId ) {
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "INSERT INTO customer_account(customer_id) VALUES (?)";
			PreparedStatement ps= conn.prepareStatement(sql);
			ps.setInt(1, customerId);
			ps.execute();
			String sql2= "INSERT INTO account_info VALUES ((SELECT MAX(account_id)"
					+ "  FROM customer_account WHERE customer_id=?),0,0)";
			PreparedStatement ps2= conn.prepareStatement(sql2);
			ps2.setInt(1, customerId);
			ps2.execute();
	}catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}

	@Override
	public boolean accountExists(int accountId) {
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "SELECT account_id FROM customer_account WHERE account_id =?";
			PreparedStatement ps= conn.prepareStatement(sql);
			ps.setInt(1, accountId);
			ResultSet results= ps.executeQuery();
			if(results.next()) {
				return true;
			}
		}	catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
	}

	@Override
	public boolean verifyJoint(String password,int accountId) {
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "SELECT password FROM customer_account ca\r\n" + 
					"RIGHT JOIN customer_info ci on ca.customer_id=ci.customer_id\r\n" + 
					"WHERE account_id=? and password= MD5(?)";
			PreparedStatement ps= conn.prepareStatement(sql);
			ps.setInt(1, accountId);
			ps.setString(2, password);
			ResultSet results= ps.executeQuery();
			if(results.next()) {
				return true;
			}
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean checkIfJoint(int accountId,int customerId) {
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "SELECT count(account_id) FROM customer_account\r\n" + 
					"WHERE account_id=? AND customer_id !=0";
			PreparedStatement ps= conn.prepareStatement(sql);
			ps.setInt(1, accountId);
			ResultSet results=ps.executeQuery();
			results.next();
			if(results.getInt(1)>1) {
				return true;
			}

			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
	}

	@Override
	public void registerReturningCredentials(int customerId) {
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "INSERT INTO customer_account(customer_id) VALUES (?)";
			PreparedStatement ps= conn.prepareStatement(sql);
			ps.setInt(1, customerId);
			ps.execute();
			String sql2= "INSERT INTO account_info VALUES ((SELECT MAX(account_id)"
					+ "  FROM customer_account WHERE customer_id=?),0,0)";
			PreparedStatement ps2= conn.prepareStatement(sql2);
			ps2.setInt(1, customerId);
			ps2.execute();
		
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean accountIsAssociated(int customerId, int accountId) {
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "SELECT account_id FROM customer_account " + 
					"WHERE account_id=? AND customer_id =?";
			PreparedStatement ps= conn.prepareStatement(sql);
			ps.setInt(1, accountId);
			ps.setInt(2, customerId);
			ResultSet results=ps.executeQuery();
			if(results.next()) {
				return true;
			}

			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
	}

	
	
	
}
