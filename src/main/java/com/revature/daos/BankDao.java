package com.revature.daos;

import java.util.List;

import com.revature.bank.beans.AccountBean;

public interface BankDao {
	
	//public void customerData();
	//view accounts will be a list of account beans
	public List<String> viewAccounts(int customerId);
	//give user choice to deposit/withdrawl from checkings/savings
	public void depositChecking(double amount,int accountId);
	public void depositSaving(double amount,int accountId);
	public void withdrawChecking(double amount,int accountId);
	public void withdrawSaving(double amount,int accountId);
	public List<AccountBean> checkBalance(String customerId);
	public AccountBean getBalance(int accountId);
	public void transferCC(int accountFro, int accountTo, double amountFro);
	public void transferCS(int accountFro, int accountTo, double amountFro);
	public void transferSC(int accountFro, int accountTo, double amountFro);
	public void transferSS(int accountFro, int accountTo, double amountFro);
	public void close(int accountId,int customerId);
	public void newAccount(int customerId );
	public boolean accountExists(int accountId);
	public boolean verifyJoint(String password, int accountId);
	public boolean checkIfJoint(int accountId,int customerId);
	public void registerReturningCredentials(int customerId);
	public boolean accountIsAssociated(int customerId,int accountId);
}
