package com.revature.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.revature.bank.beans.CustomerBean;
import com.revature.bank.util.ConnectionUtil;

public class CustomerDaoImpl implements CustomerDao{
	private ConnectionUtil connectionUtil= ConnectionUtil.getConnectionUtil();

	@Override
	public CustomerBean getUserDetails(String username, String password) {
		CustomerBean cb= new CustomerBean();
		try(Connection conn= connectionUtil.getConnection()){
			String sql="SELECT customer_id,first_name,last_name  FROM customer_info "
					+ "WHERE username=? AND password=MD5(?);";
			PreparedStatement ps= conn.prepareStatement(sql);

			ps.setString(1, username);
			ps.setString(2, password);
			ResultSet results=ps.executeQuery();
			
			if (results.next()) {
				cb.setCustomerId(results.getInt("customer_id"));
				cb.setFirstName(results.getString("first_name"));
				cb.setLastName(results.getString("last_name"));
				return cb;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean checkUsername(String username) {
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "SELECT username FROM customer_info WHERE username=?";
			PreparedStatement ps= conn.prepareStatement(sql);
			ps.setString(1, username);
			ResultSet results=ps.executeQuery();
			if (results.next()) {
				return true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public CustomerBean addUser(String firstName,String lastName,String username, String password) {
		//CustomerBean cb= new CustomerBean();
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "INSERT INTO customer_info"
					+ " (first_name,last_name,username,password)" + 
					"VALUES (?,?,?,MD5(?)) ";
			PreparedStatement ps= conn.prepareStatement(sql);
			ps.setString(1, firstName);
			ps.setString(2, lastName);
			ps.setString(3, username);
			ps.setString(4, password);
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean checkPassword(String password,String username) {
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "SELECT password FROM customer_info WHERE username=? and password=MD5(?)";
			PreparedStatement ps= conn.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, password);
			ResultSet results=ps.executeQuery();
			if (results.next()) {
				return true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	@Override
	public void newAccount(String firstName,String lastName, String username, String password) {
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "INSERT INTO customer_info (first_name,last_name,username,password) VALUES (?,?,?,MD5(?))";
			
			PreparedStatement ps=conn.prepareStatement(sql);
			
			ps.setString(1, firstName);
			ps.setString(2, lastName);
			ps.setString(3, username);
			ps.setString(4, password);
			ps.executeUpdate();
			String sql2="COMMIT;";
			PreparedStatement ps2=conn.prepareStatement(sql2);
			ps2.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void includeToAccount(int customerId, int accountId) {
		try(Connection conn= connectionUtil.getConnection()){
			String sql2="INSERT INTO customer_account(customer_id,account_id) VALUES(?,?)";
			PreparedStatement ps2= conn.prepareStatement(sql2);
			ps2.setInt(1, customerId);
			ps2.setInt(2, accountId);	
			ps2.execute();
	}catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	
}
